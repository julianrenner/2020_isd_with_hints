
def SortedRestrictedPartitions(ell,npr,t):
	assert t<=ell*npr, 'too many errors'
	#print ell,npr,t
	if ell==1:
		res = [[t]]
		return res
	else:
		ti_min = ceil(t/ell)
		ti_max = min(npr,t)
		res = []
		for ti in range(ti_min,ti_max+1):
			suffix_ti_list = SortedRestrictedPartitions(ell-1,ti,t-ti)
			for suffix in suffix_ti_list:
				res.append([ti]+suffix)
		return res

def Tset(ell,mu,r):
	if r>ell*mu:
		res = []
	else:
		if ell==1:
			res = [[r]]
		else:
			res = []
			for r1 in range(min(mu,r)+1):
				Tset_rec = Tset(ell-1,mu,r-r1)
				for r_rec in Tset_rec:
					res.append([r1]+r_rec)
	return res

def compute_svec_from_tvec(tvec,s,mu):
	svec = copy(tvec)
	ell = len(tvec)
	s -= sum(tvec)
	while s>0:
		ix_max_increase = -1
		max_increase = 0
		for i in range(ell):
			if svec[i]<mu and svec[i]/(svec[i]-tvec[i]+1)>max_increase:
				max_increase = svec[i]/(svec[i]-tvec[i]+1)
				ix_max_increase = i
		svec[ix_max_increase] += 1
		s -= 1
	return svec
	
def success_probability_given_svec(tvec,svec,npr):
	return prod([binomial(svec[i],tvec[i])/binomial(npr,tvec[i]) for i in range(len(tvec))])
	
def success_probability_maximal(tvec,npr,s):
	svec = compute_svec_from_tvec(tvec,s,npr)
	P = success_probability_given_svec(tvec,svec,npr)
	print("tvec =", tvec)
	print("svec =", svec)
	#print("%e"%(1/P))
	#print([binomial(svec[i],tvec[i])/binomial(npr,tvec[i]) for i in range(len(tvec))])
	return P

def work_factor_varying_block_size(tvec,nprvec,s,k):
	svec = copy(tvec)
	ell  = len(tvec)
	s   -= sum(tvec)
	while s>0:
		ix_max_increase = -1
		max_increase = 0
		for i in range(ell):
			if svec[i]<nprvec[i] and binomial(svec[i]+1,tvec[i])/binomial(svec[i],tvec[i])>max_increase:
				max_increase = binomial(svec[i]+1,tvec[i])/binomial(svec[i],tvec[i])	# Replace this line if another ISD algorithm is used
				ix_max_increase = i
		svec[ix_max_increase] += 1
		s -= 1
	return k^3*prod([binomial(nprvec[i],tvec[i])/binomial(svec[i],tvec[i]) for i in range(len(tvec))]) # Replace this line if another ISD algorithm is used

def proper_partition(n,ell):
	[quo,rem] = n.quo_rem(ell)
	#npr_small = floor(n/ell)
	#print(npr_small)
	#print(mod(n,ell))
	#num_npr_large = mod(n,ell)
	#prvec = [floor(n/ell)+1]*mod(n,ell)+[floor(n/ell)]*(ell-mod(n,ell))
	nprvec = [quo+1]*rem+[quo]*(ell-rem)
	indices_in_sets = [range(sum(nprvec[:i]),sum(nprvec[:i+1])) for i in range(ell)]
	return nprvec, indices_in_sets



######################################################
# Parameters
######################################################
n = 3488
k = 2720
t = 64
s = n-k
WF_Prange = k^3*binomial(n,t)/binomial(s,t)
ell_vec = [70] #range(1,101) #[5*i+1 for i in range(20)]#range(1,ell_max+1)
N = 1

WF_new_print_list = []

for ell in ell_vec:
	nprvec, indices_in_sets = proper_partition(n,ell)
	#print("ell =", ell)
	WF_new_list = []
	for sim_num in range(N):
		errs = sample(range(n),t)
		tvec = []
		for i in range(ell):
			HamWt = 0
			for j in indices_in_sets[i]:
				if j in errs:
					HamWt += 1
			tvec.append(HamWt)
		WF_new_list.append(work_factor_varying_block_size(tvec,nprvec,s,k))
	WF_new = mean(WF_new_list)
	WF_new_log = log(WF_new,2)
	print("###########################################")
	print(nprvec)
	print("n       = %3d"%(n))
	print("ell     = %3d"%(ell))
	print("npr     = %3d"%(min(nprvec)))
	#print("npr*ell = %3d"%(npr*ell))
	print("WF_new  = %f \t (Prange = %f)"%(WF_new_log, log(WF_Prange,2)))
	WF_new_print_list.append((ell,WF_new_log))
	for i in range(len(WF_new_print_list)): 
		print("%f %f"%(WF_new_print_list[i][0],WF_new_print_list[i][1])) 
	

myplot =  list_plot(WF_new_print_list, plotjoined=true, color='blue', marker='*')
myplot += list_plot([(min(ell_vec),log(WF_Prange,2)), (max(ell_vec), log(WF_Prange,2))], plotjoined=true, color='red')

myplot.show()














