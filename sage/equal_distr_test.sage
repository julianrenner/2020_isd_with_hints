

m = 11
n = 2^m
t = n/4
s = n/2



for i in range(m-3):
	ell = 2^i
	P_Prange = binomial(s,t)/binomial(n,t)
	P_blocks = (binomial(s/ell,t/ell)/binomial(n/ell,t/ell))^ell
	print("%e: %e %e"%(ell, P_Prange, P_blocks))
	
