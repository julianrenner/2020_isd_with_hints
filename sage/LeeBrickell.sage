import time

@cached_function
def mybinomial(a,b):
	return binomial(a,b)

def L(m,v):
	return sum([mybinomial(m,i) for i in range(1,v+1)])
	
def SortedRestrictedPartitions(ell,npr,t):
	assert t<=ell*npr, 'too many errors'
	#print ell,npr,t
	if ell==1:
		res = [[t]]
		return res
	else:
		ti_min = ceil(t/ell)
		ti_max = min(npr,t)
		res = []
		for ti in range(ti_min,ti_max+1):
			suffix_ti_list = SortedRestrictedPartitions(ell-1,ti,t-ti)
			for suffix in suffix_ti_list:
				res.append([ti]+suffix)
		return res

def Tset(ell,mu,r):
	if r>ell*mu:
		res = []
	else:
		if ell==1:
			res = [[r]]
		else:
			res = []
			for r1 in range(min(mu,r)+1):
				Tset_rec = Tset(ell-1,mu,r-r1)
				for r_rec in Tset_rec:
					res.append([r1]+r_rec)
	return res

def compute_svec_from_tvec(tvec,s,mu):
	svec = copy(tvec)
	ell = len(tvec)
	s -= sum(tvec)
	while s>0:
		ix_max_increase = -1
		max_increase = 0
		for i in range(ell):
			if svec[i]<mu and svec[i]/(svec[i]-tvec[i]+1)>max_increase:
				max_increase = svec[i]/(svec[i]-tvec[i]+1)
				ix_max_increase = i
		svec[ix_max_increase] += 1
		s -= 1
	return svec
	
def success_probability_given_svec(tvec,svec,npr):
	return prod([mybinomial(svec[i],tvec[i])/mybinomial(npr,tvec[i]) for i in range(len(tvec))])
	
def success_probability_maximal(tvec,npr,s):
	svec = compute_svec_from_tvec(tvec,s,npr)
	P = success_probability_given_svec(tvec,svec,npr)
	print("tvec =", tvec)
	print("svec =", svec)
	#print("%e"%(1/P))
	#print([mybinomial(svec[i],tvec[i])/mybinomial(npr,tvec[i]) for i in range(len(tvec))])
	return P

def work_factor_varying_block_size(tvec,nprvec,s,k):
	n = sum(nprvec)
	svec = copy(tvec)
	ell  = len(tvec)
	s   -= sum(tvec)
	while s>0:
		ix_max_increase = -1
		max_increase = 0
		for i in range(ell):
			if svec[i]<nprvec[i] and mybinomial(svec[i]+1,tvec[i])/mybinomial(svec[i],tvec[i])>max_increase:
				max_increase = mybinomial(svec[i]+1,tvec[i])/mybinomial(svec[i],tvec[i])	# Replace this line if another ISD algorithm is used
				ix_max_increase = i
		svec[ix_max_increase] += 1
		s -= 1
		
	#return k^3*prod([mybinomial(nprvec[i],tvec[i])/mybinomial(svec[i],tvec[i]) for i in range(len(tvec))])
	return (n-k)^2*(n+k)*prod([mybinomial(nprvec[i],tvec[i])/mybinomial(svec[i],tvec[i]) for i in range(len(tvec))]) # Replace this line if another ISD algorithm is used

def proper_partition(n,ell):
	[quo,rem] = n.quo_rem(ell)
	#npr_small = floor(n/ell)
	#print(npr_small)
	#print(mod(n,ell))
	#num_npr_large = mod(n,ell)
	#prvec = [floor(n/ell)+1]*mod(n,ell)+[floor(n/ell)]*(ell-mod(n,ell))
	nprvec = [quo+1]*rem+[quo]*(ell-rem)
	indices_in_sets = [range(sum(nprvec[:i]),sum(nprvec[:i+1])) for i in range(ell)]
	return nprvec, indices_in_sets

def Psuccess_given_x(tvec,xvec,nprvec,k,p):
	ell = len(tvec)
	Pset = Tset(ell,max(tvec),p)
	#print("|Pset|=%d"%(len(Pset)))
	return sum([prod([mybinomial(xvec[i],pvec[i])*mybinomial(nprvec[i]-xvec[i], tvec[i]-pvec[i])/mybinomial(nprvec[i], tvec[i]) for i in range(ell)]) for pvec in Pset])
	

def Psuccess_given_x_recursive(tvec,xvec,nprvec,p):
	ell = len(tvec)
	
	@cached_function
	def P_rec(local_ell,p):
		if local_ell==1:
			res = mybinomial(xvec[ell-local_ell],p)*mybinomial(nprvec[ell-local_ell]-xvec[ell-local_ell], tvec[ell-local_ell]-p)/mybinomial(nprvec[ell-local_ell], tvec[ell-local_ell])
		else:
			res = 0
			for p0 in range(p+1):
				res += mybinomial(xvec[ell-local_ell],p0)*mybinomial(nprvec[ell-local_ell]-xvec[ell-local_ell], tvec[ell-local_ell]-p0)/mybinomial(nprvec[ell-local_ell], tvec[ell-local_ell])*P_rec(local_ell-1,p-p0)
		return res
	
	return P_rec(ell,p)
	
	
def Stern_Psuccess_given_x_y_z_recursive(tvec,xvec,yvec,zvec,nprvec,p):
	ell = len(tvec)
	nu = len(zvec)
	
	@cached_function
	def P_rec(local_ell,a,b):
		if local_ell==1:
			res = mybinomial(xvec[ell-local_ell],a)*mybinomial(yvec[ell-local_ell],b)*mybinomial(nprvec[ell-local_ell]-xvec[ell-local_ell]-yvec[ell-local_ell]-zvec[ell-local_ell], tvec[ell-local_ell]-a-b)/mybinomial(nprvec[ell-local_ell], tvec[ell-local_ell])
		else:
			res = 0
			for a0 in range(a+1):
				for b0 in range(b+1):
					res += mybinomial(xvec[ell-local_ell],a0)*mybinomial(yvec[ell-local_ell],b0)*mybinomial(nprvec[ell-local_ell]-xvec[ell-local_ell]-yvec[ell-local_ell]-zvec[ell-local_ell], tvec[ell-local_ell]-a0-b0)/mybinomial(nprvec[ell-local_ell], tvec[ell-local_ell])*P_rec(local_ell-1, a-a0, b-b0)
		return res
	
	return P_rec(ell,p,p)
	

def choose_x(tvec,nprvec,k,p):
	ell = len(tvec)
	xvec = [nprvec[i]-tvec[i] for i in range(ell)]
	while sum(xvec)>k:
		Psuccess_max = Psuccess_given_x_recursive(tvec,xvec,nprvec,p)
		ix = -1
		for i in range(ell):
			xvec_i = copy(xvec)
			xvec_i[i] = xvec_i[i]-1
			Psuccess_i = Psuccess_given_x_recursive(tvec,xvec_i,nprvec,p)
			if Psuccess_i>Psuccess_max:
				ix = i
				Psuccess_max = Psuccess_i
		xvec[ix] = xvec[ix]-1
	return xvec

def LB_WF_iter(n,k,p):
	return (n-k)^2*(n+k)+(n-k)*sum([mybinomial(k,i) for i in range(1,p+1)])+(n-k)*mybinomial(k,p)

def Stern_WF_iter(xvec,yvec,zvec,n,k,t,p):
	nu = sum(zvec)
	mx = sum(xvec)
	my = sum(yvec)
	return (n-k)^2*(n+1)+nu*(L(mx,p)+L(my,p)-mx-my+mybinomial(my,p))+mybinomial(mx,p)*mybinomial(my,p)/2^(nu-1)*(t-2*p+1)*(2*p+1)


def Prange_LB_Stern(tvec,nprvec,k,debug=False):
	n = sum(nprvec)
	t = sum(tvec)
	assert k<=n, "length smaller than dimension"
	assert len(tvec)==len(nprvec), "nprvec and tvec do not have the same length"
	assert all([tvec[i]<=nprvec[i] for i in range(len(tvec))]), "some block contains more errors than its length"
	
	####################################################################
	# Prange
	####################################################################
	WF_new = work_factor_varying_block_size(tvec,nprvec,n-k,k)
	WF_new_log = log(WF_new,2)
	Prange_Summary = {"WF": WF_new_log}
	if debug:
		print("###########################################")
		print(nprvec)
		print("n       = %4d"%(n))
		print("ell     = %4d"%(ell))
		print("npr     = %4d"%(min(nprvec)))
		print("WF_new            = %f \t (Prange = %f)"%(WF_new_log, WF_log_Prange))
	
	####################################################################
	# Lee-Brickell (LB)
	####################################################################
	if debug:
		print("###########################################")
	WF_log_LB_min = 10^5
	p_WF_min = -1
	continue_with_p = True
	p = 0
	while continue_with_p:
		if p==0:
			xvec = choose_x(tvec,nprvec,k,p)
			LB_xvec = copy(xvec)
		#print("\np = %d"%(p))
		#print(tvec)
		#print(xvec)
		#print(sum(xvec), k)
		"""
		start_time = time.time()
		Psuccess = Psuccess_given_x(tvec,xvec,nprvec,k,p)
		elapsed = time.time()-start_time
		print("%e"%(Psuccess))
		print("time = %f sec"%(elapsed))
		"""
		start_time = time.time()
		Psuccess = Psuccess_given_x_recursive(tvec,xvec,nprvec,p)
		elapsed = time.time()-start_time
		#print("%e (recursive strategy)"%(Psuccess))
		#print("time = %f sec"%(elapsed))
		WF_LB = LB_WF_iter(n,k,p)/Psuccess
		#print("WF_LB_unshortened = %f"%(log(WF_LB,2)))
		if log(WF_LB,2)<WF_log_LB_min:
			WF_log_LB_min = log(WF_LB,2)
			p_WF_min = p
		if p_WF_min<p-1:
			continue_with_p = False
		p += 1
	if debug:
		print("\n###########################################")
		print("# Lee-Brickell Summary")
		print("###########################################")
		print("Minimal WF = %f \t (Prange = %f)"%(WF_log_LB_min, WF_log_Prange))
		print("Achieved for p = %d"%(p_WF_min))
		print("###########################################")

	LB_Summary = {"WF": WF_log_LB_min, "p": p_WF_min}

	#sleep(2)


	####################################################################
	# Stern
	####################################################################
	if debug:
		print("\n\n###########################################")
		print("# Stern")
		print("###########################################")
	# Take information-set distribution from Lee-Brickell for p=0 and divide
	# in each block the positions into subsets x,y evenly. For odd number of
	# positions, alternate priority
	#WF_log_Stern_min = 10^5
	#p_WF_Stern_min = -1
	#nu_WF_Stern_min = -1
	ix = 0
	xvec = []
	yvec = []
	for i in range(len(LB_xvec)):
		tmp = [ceil(LB_xvec[i]/2), floor(LB_xvec[i]/2)]
		xvec.append(tmp[ix])
		yvec.append(tmp[1-ix])
		if tmp[0]!=tmp[1]:
			ix = 1-ix
	# The distribution xvec and yvec are fixed from here on

	continue_with_p = True
	p_with_min_WF = -1
	min_WF_Stern = 10^5
	rel_tolerance = 10^-3
	p = 0
	while continue_with_p:
		zvec = [0]*len(xvec)
		nu_with_min_WF = -1
		min_WF_Stern_local = 10^5
		continue_with_nu = True
		nu = 0
		while continue_with_nu:
			Stern_Psuccess = Stern_Psuccess_given_x_y_z_recursive(tvec,xvec,yvec,zvec,nprvec,p)
			WF_Stern = Stern_WF_iter(xvec,yvec,zvec,n,k,t,p)/Stern_Psuccess
			if debug:
				print(zvec)
				#print("Psuccess(p=%3d, nu=%3d) = %e"%(p, nu, Stern_Psuccess))
				print("p=%3d, nu=%3d:\t WF = %f \t (LB = %f, Prange = %f)"%(p, nu, log(WF_Stern,2), WF_log_LB_min, WF_log_Prange))
			if log(WF_Stern,2)<(1-rel_tolerance)*min_WF_Stern_local:
				min_WF_Stern_local = log(WF_Stern,2)
				nu_with_min_WF = nu
			max_Psuccess_after_increase = 0
			ix_max_increase = 0
			for i in range(len(xvec)):
				zvec_tmp = copy(zvec)
				zvec_tmp[i] += 1
				Stern_Psuccess_tmp = Stern_Psuccess_given_x_y_z_recursive(tvec,xvec,yvec,zvec_tmp,nprvec,p)
				if Stern_Psuccess_tmp>max_Psuccess_after_increase:
					max_Psuccess_after_increase = Stern_Psuccess_tmp
					ix_max_increase = i
			zvec[ix_max_increase] += 1
			if nu_with_min_WF<nu-5:
				continue_with_nu = False
				if min_WF_Stern_local<min_WF_Stern:
					min_WF_Stern = min_WF_Stern_local
					p_with_min_WF = p
			if nu>=n-k:
				continue_with_nu = False
			nu += 1
		if debug:
			print("--------------------------------------\n")
		if p_with_min_WF<p or p>=k/2:
			continue_with_p = False
		p += 1
	if debug:
		print("\n###########################################")
		print("# Stern Summary")
		print("###########################################")
		print("Minimal WF = %f \t (Lee-Brickell = %f, Prange = %f, shortened Prange = %f)"%(min_WF_Stern_local, WF_log_LB_min, WF_log_Prange, WF_new_shortened_log))
		print("Achieved for p = %3d and nu = %3d"%(p_with_min_WF,nu_with_min_WF))
		print("###########################################")

	Stern_Summary = {"WF": min_WF_Stern, "p": p_with_min_WF, "nu": nu_with_min_WF}

	Overall_Summary = {"Prange": Prange_Summary, "LB": LB_Summary, "Stern": Stern_Summary}
	
	return Overall_Summary

def print_summary(Overall_Summary):
	print("WF_Prange = %f"%(Overall_Summary["Prange"]["WF"]))
	print("WF_LB     = %f\t (for p=%3d)"%(Overall_Summary["LB"]["WF"], Overall_Summary["LB"]["p"]))
	print("WF_Stern  = %f\t (for p=%3d and nu=%3d)"%(Overall_Summary["Stern"]["WF"], Overall_Summary["Stern"]["p"], Overall_Summary["Stern"]["nu"]))
	

######################################################
# Parameters
######################################################
n = 3488
k = 2720
t = 64
s = n-k
ell = 35

######################################################
# Partition {1,..,n} in roughly equally sizes blocks
######################################################
nprvec, indices_in_sets = proper_partition(n,ell)

######################################################
# Sample error vector and compute weight
# decomposition w.r.t. nprvec
######################################################
errs = sample(range(n),t)
tvec = []
for i in range(ell):
	HamWt = 0
	for j in indices_in_sets[i]:
		if j in errs:
			HamWt += 1
	tvec.append(HamWt)


######################################################
# Shorten blocks that have weight 0
######################################################
nprvec_shortened = []
tvec_shortened = []
k_diff = 0
ell_shortened = 0
for i in range(ell):
	if tvec[i]==0:
		k_diff += nprvec[i]
	else:
		ell_shortened += 1
		nprvec_shortened.append(nprvec[i])
		tvec_shortened.append(tvec[i])
k_shortened = k-k_diff
print("k             = %4d"%(k))
print("k_diff        = %4d"%(k_diff))
print("k_shortened   = %4d"%(k_shortened))
print("ell           = %4d"%(ell))
print("ell_shortened = %4d"%(ell_shortened))

######################################################
# Compute work factors with and without shortening
######################################################
WF_Summary_unshortened = Prange_LB_Stern(tvec,nprvec,k,debug=False)
print("########################################")
print("# Summary UNshortened")
print("########################################")
print_summary(WF_Summary_unshortened)
print("########################################")
print("# Summary shortened")
print("########################################")
if len(nprvec_shortened)==len(nprvec):
	WF_Summary_shortened = copy(WF_Summary_unshortened)
	print("same as UNshortened")
else:
	WF_Summary_shortened = Prange_LB_Stern(tvec_shortened,nprvec_shortened,k_shortened,debug=False)
	print_summary(WF_Summary_shortened)



"""
WF_Prange = k^3*mybinomial(n,t)/mybinomial(s,t)
#print("npr*ell = %3d"%(npr*ell))


WF_new_shortened = work_factor_varying_block_size(tvec_shortened,nprvec_shortened,n-k_shortened,k_shortened)


WF_new_shortened_log = log(WF_new_shortened,2)
WF_log_Prange = log(WF_Prange,2)
print("WF_new_shortened  = %f \t (Prange = %f)"%(WF_new_shortened_log, WF_log_Prange))







######################################################
# From Psuccess_given_x_recursive
######################################################

if ell==1:
	res = mybinomial(xvec[0],p)*mybinomial(nprvec[0]-xvec[0], tvec[0]-p)/mybinomial(nprvec[0], tvec[0])
else:
	res = 0
	for p0 in range(p):
		res += mybinomial(xvec[0],p0)*mybinomial(nprvec[0]-xvec[0], tvec[0]-p0)/mybinomial(nprvec[0], tvec[0])*Psuccess_given_x_recursive(tvec[1:],xvec[1:],nprvec[1:],p-p0)
return res
"""


"""

continue_with_p = True
p_with_min_WF = -1
min_WF_Stern = 10^5
p = 0
while continue_with_p:
	print(p)
	zvec = [0]*len(xvec)
	nu_with_min_WF = -1
	min_WF_Stern_local = 10^5
	continue_with_nu = True
	nu = 0
	for nu in range(ceil(log(mybinomial(k/2,p),2))+10):
		Stern_Psuccess = Stern_Psuccess_given_x_y_z_recursive(tvec,xvec,yvec,zvec,nprvec,p)
		WF_Stern = Stern_WF_iter(xvec,yvec,zvec,n,k,t,p)/Stern_Psuccess
		print(zvec)
		#print("Psuccess(p=%3d, nu=%3d) = %e"%(p, nu, Stern_Psuccess))
		print("p=%3d, nu=%3d:\t WF = %f \t (LB = %f, Prange = %f)"%(p, nu, log(WF_Stern,2), WF_log_LB_min, WF_log_Prange))
		if log(WF_Stern,2)<min_WF_Stern_local:
			min_WF_Stern_local = log(WF_Stern,2)
			p_WF_Stern_min = p
			nu_WF_Stern_min = nu
		max_Psuccess_after_increase = 0
		ix_max_increase = 0
		for i in range(len(xvec)):
			zvec_tmp = copy(zvec)
			zvec_tmp[i] += 1
			Stern_Psuccess_tmp = Stern_Psuccess_given_x_y_z_recursive(tvec,xvec,yvec,zvec_tmp,nprvec,p)
			if Stern_Psuccess_tmp>max_Psuccess_after_increase:
				max_Psuccess_after_increase = Stern_Psuccess_tmp
				ix_max_increase = i
		zvec[ix_max_increase] += 1
		if 
		nu += 1
	print("--------------------------------------\n")
	p += 1
			
"""


