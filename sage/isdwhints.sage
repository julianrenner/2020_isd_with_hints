
def SortedRestrictedPartitions(ell,npr,t):
	assert t<=ell*npr, 'too many errors'
	#print ell,npr,t
	if ell==1:
		res = [[t]]
		return res
	else:
		ti_min = ceil(t/ell)
		ti_max = min(npr,t)
		res = []
		for ti in range(ti_min,ti_max+1):
			suffix_ti_list = SortedRestrictedPartitions(ell-1,ti,t-ti)
			for suffix in suffix_ti_list:
				res.append([ti]+suffix)
		return res

def Tset(ell,mu,r):
	if r>ell*mu:
		res = []
	else:
		if ell==1:
			res = [[r]]
		else:
			res = []
			for r1 in range(min(mu,r)+1):
				Tset_rec = Tset(ell-1,mu,r-r1)
				for r_rec in Tset_rec:
					res.append([r1]+r_rec)
	return res

def compute_svec_from_tvec(tvec,s,mu):
	svec = copy(tvec)
	ell = len(tvec)
	s -= sum(tvec)
	while s>0:
		ix_max_increase = -1
		max_increase = 0
		for i in range(ell):
			if svec[i]<mu and svec[i]/(svec[i]-tvec[i]+1)>max_increase:
				max_increase = svec[i]/(svec[i]-tvec[i]+1)
				ix_max_increase = i
		svec[ix_max_increase] += 1
		s -= 1
	return svec
	
def success_probability_given_svec(tvec,svec,npr):
	return prod([binomial(svec[i],tvec[i])/binomial(npr,tvec[i]) for i in range(len(tvec))])
	
def success_probability_maximal(tvec,npr,s):
	svec = compute_svec_from_tvec(tvec,s,npr)
	P = success_probability_given_svec(tvec,svec,npr)
	print(tvec)
	print(svec)
	print("%e"%(1/P))
	print([binomial(svec[i],tvec[i])/binomial(npr,tvec[i]) for i in range(len(tvec))])
	return P


######################################################
# Parameters
######################################################
tpr = 10
spr = 15
npr = 20
ell = 5


######################################################
# Compute overall length and Prange's work factor
######################################################
t = tpr*ell
s = spr*ell
n = npr*ell
WF_Prange = binomial(n,t)/binomial(s,t)

######################################################
# Compute maximal success probabilities
######################################################
Tsett = Tset(ell,npr,t)
Tsett_size = len(Tsett)
WF_list = []
for i in range(Tsett_size):
	print("%5d of %5d"%(i+1,Tsett_size))
	tvec = Tsett[i]
	#WF_list.append([success_probability_maximal(tvec,npr,s)^(-1), tvec])
	WF_list.append(success_probability_maximal(tvec,npr,s)^(-1))

#WF_list = sorted(WF_list)
myplot  = list_plot_semilogy([(i,WF_list[i]) for i in range(Tsett_size)])
myplot += list_plot_semilogy([(0,WF_Prange), (Tsett_size-1,WF_Prange)], plotjoined=True, color="red")
myplot.show()








"""
A = (binomial(spr,tpr)/binomial(npr,tpr))^ell
B = binomial(s,t)/binomial(n,t)
print("No hint   = %e"%(B))
print("WITH hint = %e"%(A))
"""






